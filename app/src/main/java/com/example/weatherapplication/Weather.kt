package com.example.weatherapplication

import android.accounts.AuthenticatorDescription
import com.google.gson.annotations.SerializedName
import java.io.FileDescriptor

// clase response que tiene un arreglo de climas
//la palabra clave es el data que permite hacer una serializacion
data class WeatherResponse(var weather: List<Weather>?){}
class Weather (
    // si es que no se quiere poner el mismo nombre de las variables que estan en el archivo se coloca @serializadName
@SerializedName("main")
    var name: String?,
    var description:String?,
    var icon:String?
){}