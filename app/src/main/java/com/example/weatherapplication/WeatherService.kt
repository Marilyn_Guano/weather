package com.example.weatherapplication

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import  retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

// funcion
interface WeatherService {
    @GET("/data/{version}/weather")
    fun getWeather(
        @Path("version") version:String="2.5",

        @Query("q") city:String,
        @Query("appid") appId: String="213aed55aa3a8cd758f40e8020ce535a"
    ): Call<WeatherResponse>


companion object{
    val instance: WeatherService by lazy{
        val retrofit=Retrofit.Builder()
        .baseUrl("https://api.openweathermap.org")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        retrofit.create<WeatherService>(WeatherService::class.java)
    }
}}